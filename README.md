## Project Documentation

### To install a package: 
``pip install <package_name>``

### To save a package into a file:
``pip freeze > requirements.txt``

### To get the list of installed packages:
``pip list``

### To add access token to git:
``git remote set-url origin https://<USERNAME>:<ACCESS_TOKEN>@gitlab.com/<GROUP_NAME>/<PROJECT_NAME>``
- Note: For access token, you need read_api, read_repository, and write_repository


### To make the changes:
1) Add the changes by ``git add <FILE_NAME>`` (add a specific file/folder) or ``git add .`` (adds everything)
2) Optional: ``git restore --staged <FILE_NAME>`` or ``git restore --staged <FOLDER>``
3) Commit the changes by ``git commit -m ">commit_message>"``
4) Updating the remote by ``git push origin <BRANCH_NAME>``

### To install or update the project venv:
``pip install -r requirements.txt``